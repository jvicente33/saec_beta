
import java.awt.Image;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import javax.swing.JOptionPane;

public class Modificar_Compra extends javax.swing.JFrame {

    /**
     * Creates new form Modificar_Compra
     */
    public Modificar_Compra() {
        initComponents();
        setLocationRelativeTo(null);
        //Inicio de Codigo para cambiar el icono
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("Imagenes/loguis.png"));
        setIconImage(icon);
        setVisible(true);
        //Fin de Codigo para cambiar el icono
        setResizable(false);
    }
    
    public static  void modificarFichero(File FficheroNuevo, File FficheroAntiguo, String Satigualinea,String Snuevalinea){        
        try {
            if(FficheroAntiguo.exists()){
                BufferedReader Flee= new BufferedReader(new FileReader(FficheroAntiguo));
                String Slinea;
                while((Slinea=Flee.readLine())!=null) { 
                    if (Slinea.toUpperCase().trim().equals(Satigualinea.toUpperCase().trim())) {
                        EcribirFichero(FficheroNuevo,Snuevalinea);
                    }else{
                         EcribirFichero(FficheroNuevo,Slinea);
                    }             
                }

                String SnomAntiguo=FficheroAntiguo.getName();
                Flee.close();

                borrarFichero(FficheroAntiguo);

                File file=new File(SnomAntiguo);
                FficheroNuevo.renameTo(file);
                
            }else{
            }
        } catch (Exception ex) {

        }
    }
   
    
    public static void borrarFichero(File Ffichero){
        try {
         if(Ffichero.exists()){
           Ffichero.delete(); 
         }
     } catch (Exception ex) {
     }
 } 
    
    public static void EcribirFichero(File Ffichero,String SCadena){
  try {
           if(!Ffichero.exists()){
               Ffichero.createNewFile();
           }
          BufferedWriter Fescribe=new BufferedWriter(new OutputStreamWriter(new FileOutputStream(Ffichero,true), "utf-8"));
          Fescribe.write(SCadena + "\r\n");
          Fescribe.close();
       } catch (Exception ex) {
       } 
    }
    
     void cambio(){
       codigo = Cod_.getText();
       Proveedor = Proveedor_.getText();
       Articulo = Articulo_.getText();
       Cantidad = Cantidad_.getText();
       Descripcion = Descripcion_.getText();
       Factura = Factura_.getText();
       PrecioU = Precio_.getText();
       RIF = RIF_.getText();
       Tipo = Tipo_.getText();
       
//       codigo = leerID();
       
       
       
   }
   void Limpiar(){
       Proveedor_.setText("");
       Articulo_.setText("");
       Cantidad_.setText("");
       Descripcion_.setText("");
       Factura_.setText("");
       Precio_.setText("");
       RIF_.setText("");
       Tipo_.setText("");
       
       
       
   }
   
   public static void extraerDatos(int i){
        
        i = Compras.Tabla_compras.getSelectedRow();
        boolean band=false;
        
            if(i==-1){ 
                JOptionPane.showMessageDialog(null,"Seleccione una fila");
            }else{
                String codigoo = (String)Compras.Tabla_compras.getValueAt(i,0);
                String Proveedor1=(String)Compras.Tabla_compras.getValueAt(i,1); 
                String RIF1=(String)Compras.Tabla_compras.getValueAt(i,2); 
                String Articulo1=(String)Compras.Tabla_compras.getValueAt(i, 3);
                String Cantidad1 =(String)Compras.Tabla_compras.getValueAt(i, 4); 
                String PVP1=(String)Compras.Tabla_compras.getValueAt(i, 5); 
                String Tipo1=(String)Compras.Tabla_compras.getValueAt(i, 6); 
                String Factura1=(String)Compras.Tabla_compras.getValueAt(i, 7); 
                String Descripcion1=(String)Compras.Tabla_compras.getValueAt(i, 8);
                
               compras=Proveedor1+"-"+RIF1+"-"+Articulo1+"-"+Cantidad1+"-"+PVP1+"-"+Tipo1+"-"+Factura1+"-"+Descripcion1;
                
               Cod_.setText(codigoo); 
               Proveedor_.setText(Proveedor1);
               RIF_.setText(RIF1);
               Articulo_.setText(Articulo1);
               Cantidad_.setText(Cantidad1);
               Precio_.setText(PVP1);
               Tipo_.setText(Tipo1);
               Factura_.setText(Factura1);
               Descripcion_.setText(Descripcion1);
               
               
        } 
    }
    
     void Validar(){
     cambio();
        boolean[] campo = new boolean[9];
        campo[0]=Proveedor_.getText().isEmpty();
        campo[1]=Articulo_.getText().isEmpty();
        campo[2]=Cantidad_.getText().isEmpty();
        campo[3]=Precio_.getText().isEmpty();
        campo[4]=Descripcion_.getText().isEmpty();
        campo[5]=Factura_.getText().isEmpty();
        campo[6]=RIF_.getText().isEmpty();
        campo[7]=Tipo_.getText().isEmpty();
        campo[8]=Cod_.getText().isEmpty();
        
            if(campo[0] || campo[1] || campo[2] || campo[3] || campo[4]|| campo[5]|| campo[6]|| campo[7] || campo[8]){
                JOptionPane.showMessageDialog(null, "Por favor ingrese todos los campos");

            }else{
                File file=new File("Compra.txt");
                File file_aux=new File("Compra2.txt");
                String Comprass = codigo+"-"+Proveedor+"-"+RIF+"-"+Articulo+"-"+Cantidad+"-"+"-"+PrecioU+"-"+Tipo+"-"+Factura+"-"+Descripcion;
                modificarFichero(file_aux, file, compras,Comprass);
                JOptionPane.showMessageDialog(null, "Registrado Satisfactoriamente");
                Compras c = new Compras();
                c.setVisible(true);
                dispose();
               
                Limpiar();
           
        
    }
    
}
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        Cantidad_ = new javax.swing.JTextField();
        Proveedor_ = new javax.swing.JTextField();
        Descripcion_ = new javax.swing.JTextField();
        Precio_ = new javax.swing.JTextField();
        RIF_ = new javax.swing.JTextField();
        Factura_ = new javax.swing.JTextField();
        Articulo_ = new javax.swing.JTextField();
        Tipo_ = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        Cod_ = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        volver = new javax.swing.JButton();
        modificar = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Codigo");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 180, -1, 20));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("RIF/CI");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 330, -1, 20));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Articulo comprado");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 240, -1, 20));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Cantidad de Articulos");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 360, -1, 20));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Descripcion del producto");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 300, -1, 20));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("N° de factura");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 390, -1, 20));

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/limpiar.png"))); // NOI18N
        jButton2.setContentAreaFilled(false);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 470, 50, 60));
        getContentPane().add(Cantidad_, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 360, 220, -1));
        getContentPane().add(Proveedor_, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 210, 220, -1));
        getContentPane().add(Descripcion_, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 300, 220, -1));
        getContentPane().add(Precio_, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 420, 220, -1));
        getContentPane().add(RIF_, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 330, 220, -1));
        getContentPane().add(Factura_, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 390, 220, -1));
        getContentPane().add(Articulo_, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 240, 220, -1));
        getContentPane().add(Tipo_, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 270, 220, -1));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setText("Tipo de pago");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 270, -1, 20));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("Precio por unidad");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 420, 120, 20));

        Cod_.setEditable(false);
        getContentPane().add(Cod_, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 180, 50, -1));

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/grafico-circular (2)_1.png"))); // NOI18N
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 10, -1, -1));

        volver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/volverr.png"))); // NOI18N
        volver.setBorder(null);
        volver.setBorderPainted(false);
        volver.setContentAreaFilled(false);
        volver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        volver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                volverActionPerformed(evt);
            }
        });
        getContentPane().add(volver, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 40, 40));

        modificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cambiar.png"))); // NOI18N
        modificar.setBorderPainted(false);
        modificar.setContentAreaFilled(false);
        modificar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        modificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificarActionPerformed(evt);
            }
        });
        getContentPane().add(modificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 470, 60, 60));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setText("Nombre de proveedor");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 210, -1, 20));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Fondo_Inventario.png"))); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 480, 550));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Nombre de proveedor");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 210, -1, 20));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        Limpiar();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void volverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_volverActionPerformed
        Compras a= new Compras();
        this.setVisible(false);
        a.setVisible(true);
    }//GEN-LAST:event_volverActionPerformed

    private void modificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificarActionPerformed
        Validar();
        
    }//GEN-LAST:event_modificarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Modificar_Compra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Modificar_Compra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Modificar_Compra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Modificar_Compra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Modificar_Compra().setVisible(true);
            }
        });
    }
    static String compras=null;
    String codigo,Proveedor, Articulo, Descripcion, RIF, Tipo,Cantidad, PrecioU, Factura;
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JTextField Articulo_;
    public static javax.swing.JTextField Cantidad_;
    public static javax.swing.JTextField Cod_;
    public static javax.swing.JTextField Descripcion_;
    public static javax.swing.JTextField Factura_;
    public static javax.swing.JTextField Precio_;
    public static javax.swing.JTextField Proveedor_;
    public static javax.swing.JTextField RIF_;
    public static javax.swing.JTextField Tipo_;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    public static javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JButton modificar;
    private javax.swing.JButton volver;
    // End of variables declaration//GEN-END:variables
}
