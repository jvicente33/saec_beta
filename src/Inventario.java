
import java.awt.Image;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;


public class Inventario extends javax.swing.JFrame {
    
    public Inventario() {
        initComponents();
        this.setLocationRelativeTo(null);
        //Inicio de Codigo para cambiar el icono
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("Imagenes/loguis.png"));
        setIconImage(icon);
        setVisible(true);
        //Fin de Codigo para cambiar el icono
        
        //Mostramos el inventario al iniciar
        mostrarInventario();
        
        agregar.setToolTipText("Agregar Producto");
        eliminar.setToolTipText("Eliminar Producto Seleccionado");
        modificar.setToolTipText("Modificar Producto Seleccionado");
        eliminar_todo.setToolTipText("Eliminar Todo");
        volver.setToolTipText("Volver");
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tabla_inventario = new javax.swing.JTable();
        volver = new javax.swing.JButton();
        agregar = new javax.swing.JButton();
        eliminar = new javax.swing.JButton();
        modificar = new javax.swing.JButton();
        eliminar_todo = new javax.swing.JButton();
        salir = new javax.swing.JButton();
        minimizar1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(888, 583));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tabla_inventario.setFont(new java.awt.Font("Caviar Dreams", 0, 12)); // NOI18N
        tabla_inventario.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Código", "Nombre", "Marca", "Modelo", "Descripción", "Cantidad", "Precio", "Ultima Modificación"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Long.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Long.class, java.lang.Double.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tabla_inventario);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 850, 460));

        volver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/volverr.png"))); // NOI18N
        volver.setBorder(null);
        volver.setBorderPainted(false);
        volver.setContentAreaFilled(false);
        volver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        volver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                volverActionPerformed(evt);
            }
        });
        getContentPane().add(volver, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 40, 40));

        agregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/mas.png"))); // NOI18N
        agregar.setBorder(null);
        agregar.setBorderPainted(false);
        agregar.setContentAreaFilled(false);
        agregar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        agregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregarActionPerformed(evt);
            }
        });
        getContentPane().add(agregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 40, 40));

        eliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/quitar.png"))); // NOI18N
        eliminar.setBorder(null);
        eliminar.setBorderPainted(false);
        eliminar.setContentAreaFilled(false);
        eliminar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminarActionPerformed(evt);
            }
        });
        getContentPane().add(eliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 60, 40, 40));

        modificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cambiar.png"))); // NOI18N
        modificar.setBorder(null);
        modificar.setBorderPainted(false);
        modificar.setContentAreaFilled(false);
        modificar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        modificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificarActionPerformed(evt);
            }
        });
        getContentPane().add(modificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 60, 40, 40));

        eliminar_todo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cesta.png"))); // NOI18N
        eliminar_todo.setBorder(null);
        eliminar_todo.setBorderPainted(false);
        eliminar_todo.setContentAreaFilled(false);
        eliminar_todo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        eliminar_todo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminar_todoActionPerformed(evt);
            }
        });
        getContentPane().add(eliminar_todo, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 60, 40, 40));

        salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/salir.png"))); // NOI18N
        salir.setBorder(null);
        salir.setBorderPainted(false);
        salir.setContentAreaFilled(false);
        salir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });
        getContentPane().add(salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(857, 0, -1, -1));

        minimizar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/minimizarr.png"))); // NOI18N
        minimizar1.setBorder(null);
        minimizar1.setBorderPainted(false);
        minimizar1.setContentAreaFilled(false);
        minimizar1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        minimizar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                minimizar1ActionPerformed(evt);
            }
        });
        getContentPane().add(minimizar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 0, 30, -1));

        jLabel1.setFont(new java.awt.Font("Caviar Dreams", 1, 24)); // NOI18N
        jLabel1.setText("Inventario");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 10, -1, -1));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Fondo_Inventario.png"))); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void mostrarInventario() {
    tabla_inventario.getTableHeader().setReorderingAllowed(false);
    DefaultTableModel model = new DefaultTableModel(){
        
    public boolean isCellEditable(int rowIndex,int columnIndex){
        return false;} 
   
};
        model.addColumn("Código");
        model.addColumn("Nombre");
        model.addColumn("Marca");
        model.addColumn("Modelo");
        model.addColumn("Descripción");
        model.addColumn("Cantidad");
        model.addColumn("Precio");
        model.addColumn("Ultima Modificación");
        
        Inventario.tabla_inventario.setModel(model);
        
        String []datos = new String [4];
        
        try {
            File f = new File("inventario.txt");
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            String des;
            while((des = br.readLine())!= null){
                StringTokenizer linea = new StringTokenizer(des,"|");
                Vector x = new Vector();
                while(linea.hasMoreTokens()){
                    x.addElement(linea.nextToken());
                }
                model.addRow(x);

          }
        Inventario.tabla_inventario.setModel(model);
        } catch (Exception e) {
        }
    }
    
    public static  void borrarFichero(File Ffichero){
     try {
         if(Ffichero.exists()){
           Ffichero.delete();
         }
     } catch (Exception ex) {
     }
} 
     void Reseteardid(File Ffichero){
        try {
         if(Ffichero.exists()){
           BufferedWriter bw = new BufferedWriter(new FileWriter(Ffichero));
           bw.write("1");
           bw.close();
         }
     } catch (Exception ex) {
     }
        
    }
    
    public static void EcribirFichero(File Ffichero,String SCadena){
  try {
           if(!Ffichero.exists()){
               Ffichero.createNewFile();
           }
          BufferedWriter Fescribe=new BufferedWriter(new OutputStreamWriter(new FileOutputStream(Ffichero,true), "utf-8"));
          Fescribe.write(SCadena + "\r\n");
          Fescribe.close();
       } catch (Exception ex) {
       } 
}
    
    public void remove(String file, String lineToRemove) {
 
    try {
 
      File inFile = new File(file);
      
      if (!inFile.isFile()) {
        System.out.println("Parameter is not an existing file");
        return;
      }
       
      //Construct the new file that will later be renamed to the original filename. 
      File tempFile = new File(inFile.getAbsolutePath() + ".tmp");
      
      BufferedReader br = new BufferedReader(new FileReader(file));
      PrintWriter pw = new PrintWriter(new FileWriter(tempFile));
      
      String line = null;
 
      //Read from the original file and write to the new 
      //unless content matches data to be removed.
      while ((line = br.readLine()) != null) {
        
        if (!line.trim().equals(lineToRemove)) {
 
          pw.println(line);
          pw.flush();
        }
      }
      pw.close();
      br.close();
      
      //Delete the original file
      if (!inFile.delete()) {
        System.out.println("Could not delete file");
        return;
      } 
      
      //Rename the new file to the filename the original file had.
      if (!tempFile.renameTo(inFile))
        System.out.println("Could not rename file");
      
    }
    catch (FileNotFoundException ex) {
      ex.printStackTrace();
    }
    catch (IOException ex) {
      ex.printStackTrace();
    }
  }
    
    public static String extraerDatos(){
        
        int i =tabla_inventario.getSelectedRow(); 
        String inven=null;
        boolean band=false;
        
            if(i==-1){ 
                //JOptionPane.showMessageDialog(null,"Seleccione una fila");
            }else{
                String codigo=(String)tabla_inventario.getValueAt(i,0); 
                String nombre=(String)tabla_inventario.getValueAt(i,1); 
                String marca=(String)tabla_inventario.getValueAt(i, 2);
                String modelo=(String)tabla_inventario.getValueAt(i, 3); 
                String descripcion=(String)tabla_inventario.getValueAt(i, 4); 
                String cantidad=(String)tabla_inventario.getValueAt(i, 5); 
                String precio=(String)tabla_inventario.getValueAt(i, 6); 
                String fecha=(String)tabla_inventario.getValueAt(i, 7);
                
                inven=codigo+"|"+nombre+"|"+marca+"|"+modelo+"|"+descripcion+"|"+cantidad+"|"+precio+"|"+fecha;
                band=true;
                //System.out.println(inven);
        } 
            if(band==true){
            return inven;
            }else{
                return null;
            }
    }
    
   
   
    
    private void volverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_volverActionPerformed
       Admin admin= new Admin();
       this.setVisible(false);
       admin.setVisible(true);
    }//GEN-LAST:event_volverActionPerformed

    
    private void eliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminarActionPerformed
        String inventario=extraerDatos();
        File file=new File("inventario.txt");
        
        if(inventario==null){
            JOptionPane.showMessageDialog(null,"Seleccione una fila");
        }else{
        int ax = JOptionPane.showConfirmDialog(null, "¿Desea eliminar el producto?");
        if(ax == JOptionPane.YES_OPTION){
            
            remove("inventario.txt",inventario);
            
            //Eliminar fila de tabla, solo vista para el usuario
            DefaultTableModel dtm = (DefaultTableModel) tabla_inventario.getModel(); //TableProducto es el nombre de mi tabla ;) 
            dtm.removeRow(tabla_inventario.getSelectedRow()); 
            
            JOptionPane.showMessageDialog(null, "Producto eliminado con exito.");
        }else if(ax == JOptionPane.NO_OPTION){       
     }   
  }   
        
    }//GEN-LAST:event_eliminarActionPerformed

    private void modificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificarActionPerformed
        String inventario = extraerDatos();
        if(inventario==null){
          JOptionPane.showMessageDialog(null,"Seleccione la fila que desea Modificar");  
        }else{
        this.setVisible(false);
        Inventario_Modificar invm=new Inventario_Modificar();
        invm.setVisible(true);
        int i =tabla_inventario.getSelectedRow();
        Inventario_Modificar.extraerDatos(i);
        }
    }//GEN-LAST:event_modificarActionPerformed
    
    
    private void eliminar_todoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminar_todoActionPerformed
       File file=new File("inventario.txt");
        if (file.delete()){
        System.out.println("El fichero ha sido borrado satisfactoriamente");}
        else{
            System.out.println("No se ha podido eliminar");
        }
        File id = new File("idInventario.txt");
        Reseteardid(id);
        mostrarInventario();
    }//GEN-LAST:event_eliminar_todoActionPerformed

    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
        int ax = JOptionPane.showConfirmDialog(null, "¿Desea salir?");
        if(ax == JOptionPane.YES_OPTION){
            System.exit(0);
        }else if(ax == JOptionPane.NO_OPTION){       
        }     
    }//GEN-LAST:event_salirActionPerformed

    private void minimizar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_minimizar1ActionPerformed
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_minimizar1ActionPerformed

    private void agregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregarActionPerformed
        this.setVisible(false);
        Inventario_Agregar in_a =new Inventario_Agregar();
        in_a.setVisible(true);
    }//GEN-LAST:event_agregarActionPerformed

  
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Inventario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton agregar;
    private javax.swing.JButton eliminar;
    private javax.swing.JButton eliminar_todo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton minimizar1;
    private javax.swing.JButton modificar;
    private javax.swing.JButton salir;
    public static javax.swing.JTable tabla_inventario;
    private javax.swing.JButton volver;
    // End of variables declaration//GEN-END:variables
}
