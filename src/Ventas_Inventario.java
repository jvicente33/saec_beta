
import java.awt.Image;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;


public class Ventas_Inventario extends javax.swing.JFrame {
    
    public Ventas_Inventario() {
        initComponents();
        this.setLocationRelativeTo(null);
        //Inicio de Codigo para cambiar el icono
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("Imagenes/loguis.png"));
        setIconImage(icon);
        setVisible(true);
        //Fin de Codigo para cambiar el icono
        
        //Mostramos el inventario al iniciar
        mostrarInventario();
        
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tabla_inventario = new javax.swing.JTable();
        volver = new javax.swing.JButton();
        agregar = new javax.swing.JButton();
        salir = new javax.swing.JButton();
        minimizar1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(888, 583));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tabla_inventario.setFont(new java.awt.Font("Caviar Dreams", 0, 12)); // NOI18N
        tabla_inventario.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Código", "Nombre", "Marca", "Modelo", "Descripción", "Cantidad", "Precio", "Ultima Modificación"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Long.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Long.class, java.lang.Double.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tabla_inventario);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 850, 460));

        volver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/volverr.png"))); // NOI18N
        volver.setBorder(null);
        volver.setBorderPainted(false);
        volver.setContentAreaFilled(false);
        volver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        volver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                volverActionPerformed(evt);
            }
        });
        getContentPane().add(volver, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 40, 40));

        agregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/agregar-a-la-cesta-de-la-compra2.png"))); // NOI18N
        agregar.setBorder(null);
        agregar.setBorderPainted(false);
        agregar.setContentAreaFilled(false);
        agregar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        agregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregarActionPerformed(evt);
            }
        });
        getContentPane().add(agregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 40, 80, 60));

        salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/salir.png"))); // NOI18N
        salir.setBorder(null);
        salir.setBorderPainted(false);
        salir.setContentAreaFilled(false);
        salir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });
        getContentPane().add(salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(857, 0, -1, -1));

        minimizar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/minimizarr.png"))); // NOI18N
        minimizar1.setBorder(null);
        minimizar1.setBorderPainted(false);
        minimizar1.setContentAreaFilled(false);
        minimizar1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        minimizar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                minimizar1ActionPerformed(evt);
            }
        });
        getContentPane().add(minimizar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 0, 30, -1));

        jLabel1.setFont(new java.awt.Font("Caviar Dreams", 1, 24)); // NOI18N
        jLabel1.setText("Inventario");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 10, -1, -1));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Fondo_Inventario.png"))); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void mostrarInventario() {
    tabla_inventario.getTableHeader().setReorderingAllowed(false);
    DefaultTableModel model = new DefaultTableModel(){
        
    public boolean isCellEditable(int rowIndex,int columnIndex){
        return false;} 
   
};
        model.addColumn("Código");
        model.addColumn("Nombre");
        model.addColumn("Marca");
        model.addColumn("Modelo");
        model.addColumn("Descripción");
        model.addColumn("Cantidad");
        model.addColumn("Precio");
        model.addColumn("Ultima Modificación");
        
        tabla_inventario.setModel(model);
        
        String []datos = new String [4];
        
        try {
            File f = new File("inventario.txt");
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            String des;
            while((des = br.readLine())!= null){
                StringTokenizer linea = new StringTokenizer(des,"|");
                Vector x = new Vector();
                while(linea.hasMoreTokens()){
                    x.addElement(linea.nextToken());
                }
                model.addRow(x);

          }
        tabla_inventario.setModel(model);
        } catch (Exception e) {
        }
    }
    
    public static void mostrarVentas() {
    Ventas_Vender.tabla_ventas.getTableHeader().setReorderingAllowed(false);
    DefaultTableModel model = new DefaultTableModel(){
        
    public boolean isCellEditable(int rowIndex,int columnIndex){
        return false;} 
   
};
        model.addColumn("Código");
        model.addColumn("Producto");
        model.addColumn("Cant.");
        model.addColumn("Precio c/u");
        model.addColumn("Total");
        
        Ventas_Vender.tabla_ventas.setModel(model);
        
        String []datos = new String [4];
        
        String fecha=fecha();
        String nom="Ventas/ventas-"+fecha+".txt";
        
        try {
            File f = new File("ventas.txt");
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            String des;
            while((des = br.readLine())!= null){
                StringTokenizer linea = new StringTokenizer(des,"|");
                Vector x = new Vector();
                while(linea.hasMoreTokens()){
                    x.addElement(linea.nextToken());
                }
                model.addRow(x);

          }
        Ventas_Vender.tabla_ventas.setModel(model);
        } catch (Exception e) {
        }
    }
    
    public static String fecha(){
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = new GregorianCalendar();
        
        int dia=c2.get(Calendar.DATE);
        int mes=c2.get(Calendar.MONTH);
        int año=c2.get(Calendar.YEAR);
        
        String fecha=dia+"/"+(mes+1)+"/"+año;
        return fecha;
    }
    
    public static String extraerDatos(){
        
        int i =tabla_inventario.getSelectedRow(); 
        String venta=null;
        boolean band=false;
        
            if(i==-1){ 
                //JOptionPane.showMessageDialog(null,"Seleccione una fila");
            }else{
                String codigo=(String)tabla_inventario.getValueAt(i,0); 
                String nombre=(String)tabla_inventario.getValueAt(i,1); 
                String marca=(String)tabla_inventario.getValueAt(i, 2);
                String modelo=(String)tabla_inventario.getValueAt(i, 3); 
                String descripcion=(String)tabla_inventario.getValueAt(i, 4); 
                String cantidad=(String)tabla_inventario.getValueAt(i, 5); 
                String precio=(String)tabla_inventario.getValueAt(i, 6); 
                String fecha=(String)tabla_inventario.getValueAt(i, 7);
                
                String cantidad_ = JOptionPane.showInputDialog("¿Que cantidad desea vender?");
                //JOptionPane.showMessageDialog(null, "El numero ingresado es: "+ax);
                
                double cant_nro = Double.parseDouble(cantidad_);
                double precio_nro = Double.parseDouble(precio);
                double total=cant_nro*precio_nro;
                
                venta=codigo+"|"+nombre+"|"+cantidad_+"|"+precio+"|"+total;
                band=true;
                //System.out.println(inven);
        } 
            if(band==true){
            return venta;
            }else{
                return null;
            }
    }
    
   public static void agregarVentas(String inventario) {
       
        String fecha=fecha();
        String nom="ventas-"+fecha+".txt";
       
        File file= new File("ventas.txt");
        
      if(!file.exists()){
      	
      	try{
      		file.createNewFile();
                FileWriter fstream = new FileWriter(file, true);
                BufferedWriter out = new BufferedWriter(fstream);
                out.write(inventario);
                out.close();
      		
      	}catch(IOException ex){
      		ex.printStackTrace();
      	}
      }else{
      	
      	try {
            FileWriter fstream = new FileWriter(file, true);
            BufferedWriter out = new BufferedWriter(fstream);
            out.newLine();
            out.write(inventario);
            out.close();
        } catch (IOException ex) {
            
        }
      	
      }
    }
   
   public static void sumarC(){
       double sumatoria1=0.0;
        int totalRow= Ventas_Vender.tabla_ventas.getRowCount();
        totalRow-=1;
        for(int i=0;i<=(totalRow);i++)
        {
             double sumatoria= Double.parseDouble(String.valueOf(Ventas_Vender.tabla_ventas.getValueAt(i,4)));
             sumatoria1+=sumatoria;
          
          String suma=String.valueOf(sumatoria1);
          
          double iva=sumatoria1*0.12;
          String iva_=String.valueOf(iva);
         
          double total=(iva)+sumatoria1;
          String total_=String.valueOf(total);
          
          Calendar c1 = Calendar.getInstance();
        Calendar c2 = new GregorianCalendar();
        
        int dia=c2.get(Calendar.DATE);
        int mes=c2.get(Calendar.MONTH);
        int año=c2.get(Calendar.YEAR);
        
        String fecha=dia+"/"+(mes+1)+"/"+año;
          
          Ventas_Vender.mostrar.setText("Sub Total: "+suma+" Bs"+"\n"+"IVA (12%): "+iva_+" Bs"+"\nTotal: "+total_+" Bs\n"+fecha);
 
           }
   }
    
    private void volverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_volverActionPerformed
       //Admin admin= new Admin();
       this.setVisible(false);
       //admin.setVisible(true);
    }//GEN-LAST:event_volverActionPerformed

        
    
    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
        int ax = JOptionPane.showConfirmDialog(null, "¿Desea salir?");
        if(ax == JOptionPane.YES_OPTION){
            System.exit(0);
        }else if(ax == JOptionPane.NO_OPTION){       
        }     
    }//GEN-LAST:event_salirActionPerformed

    private void minimizar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_minimizar1ActionPerformed
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_minimizar1ActionPerformed

    private void agregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregarActionPerformed
        String ventas_=extraerDatos();
        
        if(ventas_==null){
            JOptionPane.showMessageDialog(null,"Seleccione una fila");
        }else{
            this.setVisible(false);
            agregarVentas(ventas_);
            mostrarVentas();
            sumarC();
            }   
    }//GEN-LAST:event_agregarActionPerformed

  
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Inventario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton agregar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton minimizar1;
    private javax.swing.JButton salir;
    public static javax.swing.JTable tabla_inventario;
    private javax.swing.JButton volver;
    // End of variables declaration//GEN-END:variables
}
