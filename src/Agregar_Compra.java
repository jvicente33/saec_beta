
import java.awt.Image;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Agregar_Compra extends javax.swing.JFrame {

   static String comprass=null;
    public Agregar_Compra() {
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        //Inicio de Codigo para cambiar el icono
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("Imagenes/loguis.png"));
        setIconImage(icon);
        setVisible(true);
        //Fin de Codigo para cambiar el icono
        String id_text=leerID();
        Cod_.setText(id_text);
    }

     public static void crearID(int id_int){
        //Creamos el archivo
        File id=new File("idCompras.txt");
        
        //Inicializamos las variables
        String id_string=String.valueOf(id_int+1);
   
        
        try{
            FileWriter escribir=new FileWriter(id,true);
            escribir.write(id_string);
            escribir.close();
        }catch(Exception e){
            
        }
    }
    public static  String leerID(){
        File Ffichero=new File("idCompras.txt");
        String aux=null;
   try {
           /*Si existe el fichero*/
           if(Ffichero.exists()){
           /*Abre un flujo de lectura a el fichero*/
           BufferedReader Flee= new BufferedReader(new FileReader(Ffichero));
           String Slinea;
           /*Lee el fichero linea a linea hasta llegar a la ultima*/
           while((Slinea=Flee.readLine())!=null) {
           /*Imprime la linea leida*/    
           aux=Slinea;         
           }
           
           //Cierra
           Flee.close();
         }else{}
 } catch (Exception ex) {}
       
   return aux;
 }
    
    public static void id() throws IOException{
       File id=new File("idCompras.txt");
        try {
         /*Si existe el fichero*/
         if(id.exists()){
           /*Borra el fichero*/  
           id.delete(); 
         }
     } catch (Exception ex) {
         /*Captura un posible error y le imprime en pantalla*/ 
     }
 
    }
    public void Registrar(String Compras){
        try {
        FileWriter fw;
        BufferedWriter bw;


        File f = new File("Compras.txt");

        if(f.exists()){

        fw = new FileWriter(f,true);
        bw = new BufferedWriter(fw);
        bw.newLine();
        bw.write(Compras);
        
        }else{
        fw = new FileWriter(f);
        bw = new BufferedWriter(fw);
        bw.write(Compras);
        }
        bw.close();
        fw.close();

        } catch (Exception e) {
            System.out.println(e);
        }
    
    }
    
    
    
     void cambio(){
       codigo = leerID();
       Proveedor = Proveedor_.getText();
       Articulo = Articulo_.getText();
       Cantidad = Cantidad_.getText();
       Descripcion = Descripcion_.getText();
       Factura = Factura_.getText();
       PrecioU = Precio_.getText();
       RIF = RIF_.getText();
       Tipo = Tipo_.getText();
       
//       codigo = leerID();
       
       
       
   }
   void Limpiar(){
       Proveedor_.setText("");
       Articulo_.setText("");
       Cantidad_.setText("");
       Descripcion_.setText("");
       Factura_.setText("");
       Precio_.setText("");
       RIF_.setText("");
       Tipo_.setText("");
       
       
       
   }
  void Validar(){
     cambio();
        boolean[] campo = new boolean[8];
        campo[0]=Proveedor_.getText().isEmpty();
        campo[1]=Articulo_.getText().isEmpty();
        campo[2]=Cantidad_.getText().isEmpty();
        campo[3]=Precio_.getText().isEmpty();
        campo[4]=Descripcion_.getText().isEmpty();
        campo[5]=Factura_.getText().isEmpty();
        campo[6]=RIF_.getText().isEmpty();
        campo[7]=Tipo_.getText().isEmpty();
        
            if(campo[0] || campo[1] || campo[2] || campo[3] || campo[4]|| campo[5]|| campo[6]|| campo[7]){
                JOptionPane.showMessageDialog(null, "Por favor ingrese todos los campos");

            }else{
               String Compras = codigo+"-"+Proveedor+"-"+RIF+"-"+Articulo+"-"+Cantidad+"-"+"-"+PrecioU+"-"+Tipo+"-"+Factura+"-"+Descripcion;
                Registrar(Compras);
               JOptionPane.showMessageDialog(null, "Registrado Satisfactoriamente");
                try {
                 id();
        } catch (IOException ex) {
           
        }
        
        int codigo_id= Integer.parseInt(codigo);
        crearID(codigo_id);
       
                Limpiar();
           
        
    }
    
}
  
 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        Cantidad_ = new javax.swing.JTextField();
        Proveedor_ = new javax.swing.JTextField();
        Descripcion_ = new javax.swing.JTextField();
        Precio_ = new javax.swing.JTextField();
        RIF_ = new javax.swing.JTextField();
        Factura_ = new javax.swing.JTextField();
        Articulo_ = new javax.swing.JTextField();
        Tipo_ = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        Cod_ = new javax.swing.JTextField();
        volver = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Codigo");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 180, -1, 20));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("RIF/CI");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 330, -1, 20));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Articulo comprado");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 240, -1, 20));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Cantidad de Articulos");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 360, -1, 20));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Descripcion del producto");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 300, -1, 20));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("N° de factura");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 390, -1, 20));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/mas.png"))); // NOI18N
        jButton1.setContentAreaFilled(false);
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 460, 50, 60));

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/limpiar.png"))); // NOI18N
        jButton2.setContentAreaFilled(false);
        jButton2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 460, 50, 60));
        getContentPane().add(Cantidad_, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 360, 220, -1));
        getContentPane().add(Proveedor_, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 210, 220, -1));
        getContentPane().add(Descripcion_, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 300, 220, -1));
        getContentPane().add(Precio_, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 420, 220, -1));
        getContentPane().add(RIF_, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 330, 220, -1));
        getContentPane().add(Factura_, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 390, 220, -1));
        getContentPane().add(Articulo_, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 240, 220, -1));
        getContentPane().add(Tipo_, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 270, 220, -1));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setText("Tipo de pago");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 270, -1, 20));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("Precio por unidad");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 420, 120, 20));

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/presentacion (2).png"))); // NOI18N
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 20, -1, -1));

        Cod_.setEditable(false);
        getContentPane().add(Cod_, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 180, 50, -1));

        volver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/volverr.png"))); // NOI18N
        volver.setBorder(null);
        volver.setBorderPainted(false);
        volver.setContentAreaFilled(false);
        volver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        volver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                volverActionPerformed(evt);
            }
        });
        getContentPane().add(volver, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 40, 40));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setText("Nombre de proveedor");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 210, -1, 20));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Fondo_Inventario.png"))); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 470, 550));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Nombre de proveedor");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 210, -1, 20));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Validar();
        cambio();
       int ax = JOptionPane.showConfirmDialog(null, "¿El Articulo esta en el Inventario?");
        if(ax == JOptionPane.YES_OPTION){
                    int ex = JOptionPane.showConfirmDialog(null, "¿Desea Modificarlo?");
                        if(ex == JOptionPane.YES_OPTION){
                        Inventario2 ia = new Inventario2();
                        ia.setVisible(true);
                        dispose();
                         
            
                        }else if(ex == JOptionPane.NO_OPTION){
        }
            }else if(ax == JOptionPane.NO_OPTION){
                    int ux = JOptionPane.showConfirmDialog(null, "¿Desea Agregarlo?");
                    if(ux == JOptionPane.YES_OPTION){
                        Inventario_Agregar ia = new Inventario_Agregar();
                        ia.setVisible(true);
                        dispose();
                        
           
        }else if(ax == JOptionPane.NO_OPTION){
            Compras c = new Compras();
            c.setVisible(true);
            dispose();
        }
        }
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void volverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_volverActionPerformed
        Admin a= new Admin();
        this.setVisible(false);
        a.setVisible(true);
    }//GEN-LAST:event_volverActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
     Limpiar();
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Agregar_Compra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Agregar_Compra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Agregar_Compra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Agregar_Compra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Agregar_Compra().setVisible(true);
            }
        });
    }
     String codigo,Proveedor, Articulo, Descripcion, RIF, Tipo,Cantidad, PrecioU, Factura;
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JTextField Articulo_;
    public static javax.swing.JTextField Cantidad_;
    public static javax.swing.JTextField Cod_;
    public static javax.swing.JTextField Descripcion_;
    public static javax.swing.JTextField Factura_;
    public static javax.swing.JTextField Precio_;
    public static javax.swing.JTextField Proveedor_;
    public static javax.swing.JTextField RIF_;
    public static javax.swing.JTextField Tipo_;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    public static javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JButton volver;
    // End of variables declaration//GEN-END:variables
}
