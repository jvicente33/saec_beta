
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.Position;
import javax.swing.text.Segment;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import java.io.FileOutputStream;

public class Ventas_Vender extends javax.swing.JFrame {
    
    public Ventas_Vender() {
        initComponents();
        this.setLocationRelativeTo(null);
        //Inicio de Codigo para cambiar el icono
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("Imagenes/loguis.png"));
        setIconImage(icon);
        setVisible(true);
        //Fin de Codigo para cambiar el icono
        
        check();
    
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        salir = new javax.swing.JButton();
        minimizar1 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        cliente = new javax.swing.JTextField();
        agregar_cliente = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        cliente_existente = new javax.swing.JCheckBox();
        cliente_nuevo = new javax.swing.JCheckBox();
        empleado = new javax.swing.JTextField();
        buscar_empleado = new javax.swing.JButton();
        buscar_cliente = new javax.swing.JButton();
        fecha = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabla_ventas = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        mostrar = new javax.swing.JTextArea();
        limpiar = new javax.swing.JButton();
        agregar_inventario = new javax.swing.JButton();
        vender = new javax.swing.JButton();
        cancelar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(888, 583));
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/salir.png"))); // NOI18N
        salir.setBorder(null);
        salir.setBorderPainted(false);
        salir.setContentAreaFilled(false);
        salir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });
        getContentPane().add(salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(857, 0, -1, -1));

        minimizar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/minimizarr.png"))); // NOI18N
        minimizar1.setBorder(null);
        minimizar1.setBorderPainted(false);
        minimizar1.setContentAreaFilled(false);
        minimizar1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        minimizar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                minimizar1ActionPerformed(evt);
            }
        });
        getContentPane().add(minimizar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 0, 30, -1));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cartel-de-se-vende.png"))); // NOI18N
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 290, 200));

        jLabel1.setFont(new java.awt.Font("Caviar Dreams", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Cliente:");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 110, 80, -1));

        cliente.setEditable(false);
        cliente.setBackground(new java.awt.Color(255, 255, 255));
        cliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clienteActionPerformed(evt);
            }
        });
        getContentPane().add(cliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 140, 290, -1));

        agregar_cliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/masmas.png"))); // NOI18N
        agregar_cliente.setBorderPainted(false);
        agregar_cliente.setContentAreaFilled(false);
        agregar_cliente.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        agregar_cliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregar_clienteActionPerformed(evt);
            }
        });
        getContentPane().add(agregar_cliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 140, 20, 20));

        jLabel4.setFont(new java.awt.Font("Caviar Dreams", 1, 14)); // NOI18N
        jLabel4.setText("Vendedor:");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 60, -1, -1));

        cliente_existente.setFont(new java.awt.Font("Caviar Dreams", 0, 12)); // NOI18N
        cliente_existente.setText("Existente");
        cliente_existente.setContentAreaFilled(false);
        cliente_existente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cliente_existenteMousePressed(evt);
            }
        });
        getContentPane().add(cliente_existente, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 110, -1, 20));

        cliente_nuevo.setFont(new java.awt.Font("Caviar Dreams", 0, 12)); // NOI18N
        cliente_nuevo.setText("Nuevo");
        cliente_nuevo.setContentAreaFilled(false);
        cliente_nuevo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cliente_nuevoMousePressed(evt);
            }
        });
        cliente_nuevo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cliente_nuevoKeyPressed(evt);
            }
        });
        getContentPane().add(cliente_nuevo, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 110, -1, 20));

        empleado.setEditable(false);
        empleado.setBackground(new java.awt.Color(255, 255, 255));
        empleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                empleadoActionPerformed(evt);
            }
        });
        getContentPane().add(empleado, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 60, 290, -1));

        buscar_empleado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/busqueda3.png"))); // NOI18N
        buscar_empleado.setBorderPainted(false);
        buscar_empleado.setContentAreaFilled(false);
        buscar_empleado.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        buscar_empleado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscar_empleadoActionPerformed(evt);
            }
        });
        getContentPane().add(buscar_empleado, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 60, 20, 20));

        buscar_cliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/busqueda3.png"))); // NOI18N
        buscar_cliente.setBorderPainted(false);
        buscar_cliente.setContentAreaFilled(false);
        buscar_cliente.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        buscar_cliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscar_clienteActionPerformed(evt);
            }
        });
        getContentPane().add(buscar_cliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 140, 20, 20));
        getContentPane().add(fecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 10, 130, 20));

        tabla_ventas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Código", "Producto", "Cant.", "Precio c/u", "Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tabla_ventas);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 210, 770, 250));

        mostrar.setEditable(false);
        mostrar.setColumns(20);
        mostrar.setFont(new java.awt.Font("Caviar Dreams", 0, 12)); // NOI18N
        mostrar.setRows(5);
        jScrollPane2.setViewportView(mostrar);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 470, 370, 100));

        limpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/limpiar.png"))); // NOI18N
        limpiar.setBorderPainted(false);
        limpiar.setContentAreaFilled(false);
        limpiar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                limpiarActionPerformed(evt);
            }
        });
        getContentPane().add(limpiar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 280, -1, -1));

        agregar_inventario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/agregar-al-portapapeles-simbolo-de-interfaz-negro.png"))); // NOI18N
        agregar_inventario.setBorderPainted(false);
        agregar_inventario.setContentAreaFilled(false);
        agregar_inventario.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        agregar_inventario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregar_inventarioActionPerformed(evt);
            }
        });
        getContentPane().add(agregar_inventario, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, -1, -1));

        vender.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/vender2.png"))); // NOI18N
        vender.setBorderPainted(false);
        vender.setContentAreaFilled(false);
        vender.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        vender.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                venderActionPerformed(evt);
            }
        });
        getContentPane().add(vender, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 510, 50, 40));

        cancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/error.png"))); // NOI18N
        cancelar.setBorderPainted(false);
        cancelar.setContentAreaFilled(false);
        cancelar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelarActionPerformed(evt);
            }
        });
        getContentPane().add(cancelar, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 510, 50, 40));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Fondo_Inventario.png"))); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void check(){
        agregar_cliente.setEnabled(false);
        buscar_cliente.setEnabled(false);
    }
   
    public static  void borrarFichero(File Ffichero){
     try {
         if(Ffichero.exists()){
           Ffichero.delete();
         }
     } catch (Exception ex) {
     }
} 
    
    public static void EcribirFichero(File Ffichero,String SCadena){
  try {
           if(!Ffichero.exists()){
               Ffichero.createNewFile();
           }
          BufferedWriter Fescribe=new BufferedWriter(new OutputStreamWriter(new FileOutputStream(Ffichero,true), "utf-8"));
          Fescribe.write(SCadena + "\r\n");
          Fescribe.close();
       } catch (Exception ex) {
       } 
}
   
 
        
    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
        int ax = JOptionPane.showConfirmDialog(null, "¿Desea salir?");
        if(ax == JOptionPane.YES_OPTION){
            System.exit(0);
        }else if(ax == JOptionPane.NO_OPTION){       
        }     
    }//GEN-LAST:event_salirActionPerformed

    private void minimizar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_minimizar1ActionPerformed
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_minimizar1ActionPerformed

    private void clienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_clienteActionPerformed

    private void empleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_empleadoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_empleadoActionPerformed

    private void buscar_empleadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscar_empleadoActionPerformed
       Ventas_Empleados ven_em=new Ventas_Empleados();
       //this.setVisible(false);
       ven_em.setVisible(true);
    }//GEN-LAST:event_buscar_empleadoActionPerformed

    private void cliente_nuevoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cliente_nuevoKeyPressed
        
    }//GEN-LAST:event_cliente_nuevoKeyPressed

    private void cliente_nuevoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cliente_nuevoMousePressed
        agregar_cliente.setEnabled(true);
        buscar_cliente.setEnabled(false);
        cliente_existente.setSelected(false);
    }//GEN-LAST:event_cliente_nuevoMousePressed

    private void cliente_existenteMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cliente_existenteMousePressed
        agregar_cliente.setEnabled(false);
        buscar_cliente.setEnabled(true);
        cliente_nuevo.setSelected(false);
    }//GEN-LAST:event_cliente_existenteMousePressed

    private void buscar_clienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscar_clienteActionPerformed
        //this.setVisible(false);
        Ventas_Clientes ve_cli=new Ventas_Clientes();
        ve_cli.setVisible(true);
    }//GEN-LAST:event_buscar_clienteActionPerformed

    private void agregar_clienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregar_clienteActionPerformed
        //this.setVisible(false);
        Ventas_Clientes_Agregar ve_a=new Ventas_Clientes_Agregar();
        ve_a.setVisible(true);
    }//GEN-LAST:event_agregar_clienteActionPerformed

    private void agregar_inventarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregar_inventarioActionPerformed
        Ventas_Inventario ve_in=new Ventas_Inventario();
        ve_in.setVisible(true);
    }//GEN-LAST:event_agregar_inventarioActionPerformed

    private void limpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_limpiarActionPerformed
        File file=new File("ventas.txt");
        file.delete();
        /*if (file.delete()){
        System.out.println("El fichero ha sido borrado satisfactoriamente");}
        else{
            System.out.println("No se ha podido eliminar");
        }*/
        tabla_ventas.setModel(new DefaultTableModel());
        mostrar.setText("");
        //Ventas_Inventario.mostrarVentas();
    }//GEN-LAST:event_limpiarActionPerformed

    private void cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelarActionPerformed
       this.setVisible(false);
       Admin ad=new Admin();
       ad.setVisible(true);
    }//GEN-LAST:event_cancelarActionPerformed

    private void venderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_venderActionPerformed
        
        String empleado_=null;
                empleado_= empleado.getText();
        String cliente_=null;
                cliente_=cliente.getText();
        String mostrar_=null;
                mostrar_=mostrar.getText();
                
        JOptionPane.showMessageDialog(null, "Venta realizada con Exito");    
        JOptionPane.showMessageDialog(null, empleado_+"\n"+cliente_+"\n"+mostrar_);
        File file_=new File("ventas.txt");
        borrarFichero(file_);
        tabla_ventas.setModel(new DefaultTableModel());
        mostrar.setText("");
        empleado.setText("");
        cliente.setText("");
        cliente_existente.setSelected(false);
        cliente_nuevo.setSelected(false);
        check();
    }//GEN-LAST:event_venderActionPerformed
    
            
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ventas_Vender().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton agregar_cliente;
    private javax.swing.JButton agregar_inventario;
    public static javax.swing.JButton buscar_cliente;
    private javax.swing.JButton buscar_empleado;
    private javax.swing.JButton cancelar;
    public static javax.swing.JTextField cliente;
    public static javax.swing.JCheckBox cliente_existente;
    public static javax.swing.JCheckBox cliente_nuevo;
    public static javax.swing.JTextField empleado;
    private javax.swing.JLabel fecha;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton limpiar;
    private javax.swing.JButton minimizar1;
    public static javax.swing.JTextArea mostrar;
    private javax.swing.JButton salir;
    public static javax.swing.JTable tabla_ventas;
    private javax.swing.JButton vender;
    // End of variables declaration//GEN-END:variables
}
