


import java.awt.Image;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;
public class Ventas_Clientes_Agregar extends javax.swing.JFrame {

   String codigo,Descripcion,Direccion, Correo, Rif, Telefono, Fax, Website, Fecha;
   
    public Ventas_Clientes_Agregar() {
       initComponents();  
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle("Registro de Usuario");
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("Imagenes/loguis.png"));
        setIconImage(icon);
        setVisible(true);
        fecha.setToolTipText("Opcional");
        WebSite.setToolTipText("Opcional");
        //Fin de Codigo para cambiar el icono
        String id_text=leerID();
        Cod.setText(id_text);
        
        volver.setToolTipText("Volver");
        Agregar.setToolTipText("Agregar Cliente");
        Limpiar.setToolTipText("Limpiar los campos de texto");
    }
     public static void crearID(int id_int){
        //Creamos el archivo
        File id=new File("idClientes.txt");
        
        //Inicializamos las variables
        String id_string=String.valueOf(id_int+1);
   
        
        try{
            FileWriter escribir=new FileWriter(id,true);
            escribir.write(id_string);
            escribir.close();
        }catch(Exception e){
            
        }
    }
    public static  String leerID(){
        File Ffichero=new File("idClientes.txt");
        String aux=null;
   try {
           /*Si existe el fichero*/
           if(Ffichero.exists()){
           /*Abre un flujo de lectura a el fichero*/
           BufferedReader Flee= new BufferedReader(new FileReader(Ffichero));
           String Slinea;
           /*Lee el fichero linea a linea hasta llegar a la ultima*/
           while((Slinea=Flee.readLine())!=null) {
           /*Imprime la linea leida*/    
           aux=Slinea;         
           }
           
           //Cierra
           Flee.close();
         }else{}
 } catch (Exception ex) {}
       
   return aux;
 }
    
    public static void id() throws IOException{
       File id=new File("idClientes.txt");
        try {
         /*Si existe el fichero*/
         if(id.exists()){
           /*Borra el fichero*/  
           id.delete(); 
         }
     } catch (Exception ex) {
         /*Captura un posible error y le imprime en pantalla*/ 
     }
 
    }
    
   public void Registrar(String Clientes){
        try {
        FileWriter fw;
        BufferedWriter bw;


        File f = new File("RegistroClientes.txt");

        if(f.exists()){

        fw = new FileWriter(f,true);
        bw = new BufferedWriter(fw);
        bw.write(Clientes);
        bw.newLine();
        }else{
        fw = new FileWriter(f);
        bw = new BufferedWriter(fw);
        bw.write(Clientes);
        }
        bw.close();
        fw.close();

        } catch (Exception e) {
            System.out.println(e);
        }
    
    }
    
   void cambio(){
       codigo = leerID();
       Descripcion = Descripcion_.getText();
       Direccion= Direccion_.getText();
       Rif=Rif_.getText();
       Telefono=Telefono_.getText();
       Fax = Fax_.getText();
       Correo= Email_.getText();
       Website = Web_.getText();
       Fecha = FechaNacimiento_.getText();
   }
   void Limpiar(){
          
       Descripcion_.setText("");
       Direccion_.setText("");
       Rif_.setText("");
       Telefono_.setText("");
       Fax_.setText("");
       Email_.setText("");
       Web_.setText("");
       FechaNacimiento_.setText("");
       Cod.setText(leerID());
      
      
       
   }
  void Validar(){
     cambio();
     boolean[] campo = new boolean[6];
        campo[0]=Descripcion_.getText().isEmpty();
        campo[1]=Direccion_.getText().isEmpty();
        campo[2]=Rif_.getText().isEmpty();
        campo[3]=Telefono_.getText().isEmpty();
        campo[4]=Fax_.getText().isEmpty();
        campo[5]=Email_.getText().isEmpty();
            if(campo[0] || campo[1] || campo[2] || campo[3] || campo[4]|| campo[5]){
                JOptionPane.showMessageDialog(null, "Por favor ingrese todos los campos");

            }else{
                String Clientes = codigo+"-"+Descripcion+"-"+Direccion+"-"+Rif+"-"+Telefono+"-"+Fax+"-"+Correo+"-"+Website+"-"+Fecha;
                Registrar(Clientes);
                JOptionPane.showMessageDialog(null, "Registrado Satisfactoriamente");
                try {
                 id();
        } catch (IOException ex) {
           
        }
        
        int codigo_id= Integer.parseInt(codigo);
        crearID(codigo_id);
       
                Limpiar();
           
        
    }
    
}


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Direccion_ = new javax.swing.JTextField();
        Descripcion_ = new javax.swing.JTextField();
        Rif_ = new javax.swing.JTextField();
        Telefono_ = new javax.swing.JTextField();
        Fax_ = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        Email = new javax.swing.JLabel();
        WebSite = new javax.swing.JLabel();
        fecha = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        Cod = new javax.swing.JTextField();
        FechaNacimiento_ = new javax.swing.JTextField();
        Web_ = new javax.swing.JTextField();
        Email_ = new javax.swing.JTextField();
        volver = new javax.swing.JButton();
        Limpiar = new javax.swing.JButton();
        Agregar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        getContentPane().add(Direccion_, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 150, 420, -1));
        getContentPane().add(Descripcion_, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 120, 420, -1));
        getContentPane().add(Rif_, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 180, 140, -1));
        getContentPane().add(Telefono_, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 210, 140, -1));
        getContentPane().add(Fax_, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 240, 140, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Fax");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 240, -1, 20));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Telefono");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 210, -1, 20));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("RIF/CI");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 180, -1, 20));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Direccion");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 150, -1, 20));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Descripcion");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 120, -1, 20));

        Email.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        Email.setText("Email");
        getContentPane().add(Email, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 340, -1, 20));

        WebSite.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        WebSite.setText("Website");
        getContentPane().add(WebSite, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 370, -1, 20));

        fecha.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        fecha.setText("Fecha de Nacimiento");
        getContentPane().add(fecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 400, -1, 20));

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setText("Codigo");
        getContentPane().add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, -1, 20));

        Cod.setEditable(false);
        Cod.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CodActionPerformed(evt);
            }
        });
        getContentPane().add(Cod, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 90, 100, -1));
        getContentPane().add(FechaNacimiento_, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 400, 116, -1));
        getContentPane().add(Web_, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 370, 116, -1));
        getContentPane().add(Email_, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 340, 116, -1));

        volver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/volverr.png"))); // NOI18N
        volver.setBorderPainted(false);
        volver.setContentAreaFilled(false);
        volver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        volver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                volverActionPerformed(evt);
            }
        });
        getContentPane().add(volver, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 40, 30));

        Limpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/limpiar.png"))); // NOI18N
        Limpiar.setBorderPainted(false);
        Limpiar.setContentAreaFilled(false);
        Limpiar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                LimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(Limpiar, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 480, -1, -1));

        Agregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/mas.png"))); // NOI18N
        Agregar.setBorderPainted(false);
        Agregar.setContentAreaFilled(false);
        Agregar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Agregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgregarActionPerformed(evt);
            }
        });
        getContentPane().add(Agregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 470, 80, 60));

        jLabel6.setFont(new java.awt.Font("Caviar Dreams", 1, 18)); // NOI18N
        jLabel6.setText("Registro de Clientes");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 10, -1, -1));

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/equipo (1)_1.png"))); // NOI18N
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 280, -1, -1));

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Fondo_Inventario.png"))); // NOI18N
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 580, 560));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void volverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_volverActionPerformed
        Clientes cl=new Clientes();
        cl.setVisible(true);
        dispose();
    }//GEN-LAST:event_volverActionPerformed

    private void LimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_LimpiarActionPerformed
        Limpiar();
    }//GEN-LAST:event_LimpiarActionPerformed

    private void AgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgregarActionPerformed
        Validar();
        cambio();
        this.setVisible(false);
        Ventas_Vender ve=new Ventas_Vender();
        ve.setVisible(true);
        
        String nombre=Descripcion_.getText();
        Ventas_Vender.cliente.setText(nombre);
   

    }//GEN-LAST:event_AgregarActionPerformed

    private void CodActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CodActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CodActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegistroClientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegistroClientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegistroClientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegistroClientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RegistroClientes().setVisible(true);
            }
        });
    }

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Agregar;
    private javax.swing.JTextField Cod;
    private javax.swing.JTextField Descripcion_;
    private javax.swing.JTextField Direccion_;
    private javax.swing.JLabel Email;
    private javax.swing.JTextField Email_;
    private javax.swing.JTextField Fax_;
    private javax.swing.JTextField FechaNacimiento_;
    private javax.swing.JButton Limpiar;
    private javax.swing.JTextField Rif_;
    private javax.swing.JTextField Telefono_;
    private javax.swing.JLabel WebSite;
    private javax.swing.JTextField Web_;
    private javax.swing.JLabel fecha;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JButton volver;
    // End of variables declaration//GEN-END:variables
}
