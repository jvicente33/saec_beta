
import java.awt.Image;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


public class Inventario_Agregar extends javax.swing.JFrame {
    
    public Inventario_Agregar() {
        initComponents();
        this.setLocationRelativeTo(null);
        //Inicio de Codigo para cambiar el icono
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("Imagenes/loguis.png"));
        setIconImage(icon);
        setVisible(true);
        //Fin de Codigo para cambiar el icono
        
        //Leer ID
        String id_text=leerID();
        codigo.setText(id_text);
        
        agregar.setToolTipText("Agregar Producto");
        volver.setToolTipText("Volver al Inventario");
        limpiar.setToolTipText("Limpiar campos de texto");
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        nombre = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        marca = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        modelo = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        codigo = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        precio = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        cant = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        descripcion = new javax.swing.JTextArea();
        volver = new javax.swing.JButton();
        limpiar = new javax.swing.JButton();
        agregar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Agregar Inventario.png"))); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 40, -1, -1));

        jLabel3.setFont(new java.awt.Font("Caviar Dreams", 1, 14)); // NOI18N
        jLabel3.setText("Agregar Inventario");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 20, -1, -1));

        jLabel4.setFont(new java.awt.Font("Caviar Dreams", 0, 12)); // NOI18N
        jLabel4.setText("Modelo:");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 220, -1, 20));

        nombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nombreActionPerformed(evt);
            }
        });
        getContentPane().add(nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 180, 100, -1));

        jLabel5.setFont(new java.awt.Font("Caviar Dreams", 0, 12)); // NOI18N
        jLabel5.setText("Nombre del Producto:");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 180, -1, 20));
        getContentPane().add(marca, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 220, 120, -1));

        jLabel6.setFont(new java.awt.Font("Caviar Dreams", 0, 12)); // NOI18N
        jLabel6.setText("Descripción:");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 300, -1, 20));
        getContentPane().add(modelo, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 220, 140, -1));

        jLabel7.setFont(new java.awt.Font("Caviar Dreams", 0, 12)); // NOI18N
        jLabel7.setText("Marca:");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, -1, 20));

        codigo.setEnabled(false);
        codigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                codigoActionPerformed(evt);
            }
        });
        getContentPane().add(codigo, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 260, 120, -1));

        jLabel8.setFont(new java.awt.Font("Caviar Dreams", 0, 12)); // NOI18N
        jLabel8.setText("Código:");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 260, -1, 20));
        getContentPane().add(precio, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 260, 140, -1));

        jLabel9.setFont(new java.awt.Font("Caviar Dreams", 0, 12)); // NOI18N
        jLabel9.setText("Precio:");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 260, -1, 20));

        cant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cantActionPerformed(evt);
            }
        });
        getContentPane().add(cant, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 180, 80, -1));

        jLabel10.setFont(new java.awt.Font("Caviar Dreams", 0, 12)); // NOI18N
        jLabel10.setText("Cantidad:");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 180, -1, 20));

        descripcion.setColumns(20);
        descripcion.setRows(5);
        jScrollPane1.setViewportView(descripcion);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 300, 300, 60));

        volver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/volverr.png"))); // NOI18N
        volver.setBorderPainted(false);
        volver.setContentAreaFilled(false);
        volver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        volver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                volverActionPerformed(evt);
            }
        });
        getContentPane().add(volver, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 40, 30));

        limpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/limpiar.png"))); // NOI18N
        limpiar.setBorderPainted(false);
        limpiar.setContentAreaFilled(false);
        limpiar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                limpiarActionPerformed(evt);
            }
        });
        getContentPane().add(limpiar, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 390, 50, -1));

        agregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/mas.png"))); // NOI18N
        agregar.setBorderPainted(false);
        agregar.setContentAreaFilled(false);
        agregar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        agregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregarActionPerformed(evt);
            }
        });
        getContentPane().add(agregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 390, 50, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fondo1.jpg"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 410, 450));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void id() throws IOException{
       File id=new File("id.txt");
       borrarFichero(id); 
    }
    
    public static void borrarFichero(File Ffichero){
     
        try {
         /*Si existe el fichero*/
         if(Ffichero.exists()){
           /*Borra el fichero*/  
           Ffichero.delete(); 
         }
     } catch (Exception ex) {
         /*Captura un posible error y le imprime en pantalla*/ 
     }
 } 
    
    public static void crearID(int id_int){
        //Creamos el archivo
        File id=new File("id.txt");
        
        //Inicializamos las variables
        String id_string=String.valueOf(id_int+1);
   
        
        try{
            FileWriter escribir=new FileWriter(id,true);
            escribir.write(id_string);
            escribir.close();
        }catch(Exception e){
            
        }
    }
    
    public static  String leerID(){
        File Ffichero=new File("id.txt");
        String aux=null;
   try {
       /*Si existe el fichero*/
       if(Ffichero.exists()){
           /*Abre un flujo de lectura a el fichero*/
           BufferedReader Flee= new BufferedReader(new FileReader(Ffichero));
           String Slinea;
           /*Lee el fichero linea a linea hasta llegar a la ultima*/
           while((Slinea=Flee.readLine())!=null) {
           /*Imprime la linea leida*/    
           aux=Slinea;         
           }
           
           //Cierra
           Flee.close();
         }else{}
   } catch (Exception ex) {}
       
   return aux;
 }
    public static void limpiar(){
       nombre.setText("");
       cant.setText("");
       marca.setText("");
       modelo.setText("");
       codigo.setText(leerID());
       descripcion.setText("");
       precio.setText("");
    }
    
    public static void agregarInventario(String inventario) {
        
        File file= new File("inventario.txt");
        
      if(!file.exists()){
      	
      	try{
      		file.createNewFile();
                FileWriter fstream = new FileWriter(file, true);
                BufferedWriter out = new BufferedWriter(fstream);
                out.write(inventario);
                out.close();
      		
      	}catch(IOException ex){
      		ex.printStackTrace();
      	}
      }else{
      	
      	try {
            FileWriter fstream = new FileWriter(file, true);
            BufferedWriter out = new BufferedWriter(fstream);
            out.newLine();
            out.write(inventario);
            out.close();
        } catch (IOException ex) {
            
        }
      	
      }
    }
  
    
    private void nombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nombreActionPerformed

    private void cantActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cantActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cantActionPerformed

    private void volverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_volverActionPerformed
       this.setVisible(false);
       Inventario in=new Inventario();
       in.setVisible(true);
    }//GEN-LAST:event_volverActionPerformed

    private void limpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_limpiarActionPerformed
       limpiar();
    }//GEN-LAST:event_limpiarActionPerformed
    
    private void agregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregarActionPerformed
       if((nombre.getText().length()==0) || (codigo.getText().length()==0) || (marca.getText().length()==0) || (modelo.getText().length()==0) || (cant.getText().length()==0) || (descripcion.getText().length()==0) || (precio.getText().length()==0)){
               JOptionPane.showMessageDialog(null, "Por favor llene todos los cuadros");
       }else{
           //Crear variables para guardar lo que hay en los jtextfield
        String nombree, cantidad, marcaa, modeloo, codigoo,precioo, descripcionn;
        
        //Obtenemos la fecha del sistema
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = new GregorianCalendar();
        
        int dia=c2.get(Calendar.DATE);
        int mes=c2.get(Calendar.MONTH);
        int año=c2.get(Calendar.YEAR);
        
        String fecha=dia+"/"+(mes+1)+"/"+año;
        //Guardar los jtextfield en las variables antes creadas
        nombree=nombre.getText();
        cantidad=cant.getText();
        marcaa=marca.getText();
        modeloo=modelo.getText();
        codigoo=leerID();
        precioo=precio.getText();
        descripcionn=descripcion.getText();
        
        //Crear una nueva variable y unir todo
        String inven=codigoo+"|"+nombree+"|"+marcaa+"|"+modeloo+"|"+descripcionn+"|"+cantidad+"|"+precioo+"|"+fecha;
        //System.out.println(inven);
        
        agregarInventario(inven);
        
        try {
            id();
        } catch (IOException ex) {
            Logger.getLogger(Inventario_Agregar.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        int codigo_id= Integer.parseInt(codigoo);
        crearID(codigo_id);
        
        limpiar();
       }
    }//GEN-LAST:event_agregarActionPerformed

    private void codigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_codigoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_codigoActionPerformed

    
    public static void main(String args[]) {
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Inventario_Agregar().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton agregar;
    private static javax.swing.JTextField cant;
    private static javax.swing.JTextField codigo;
    private static javax.swing.JTextArea descripcion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton limpiar;
    private static javax.swing.JTextField marca;
    private static javax.swing.JTextField modelo;
    private static javax.swing.JTextField nombre;
    private static javax.swing.JTextField precio;
    private javax.swing.JButton volver;
    // End of variables declaration//GEN-END:variables
}
