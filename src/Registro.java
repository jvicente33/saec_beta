import java.awt.Image;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Registro extends javax.swing.JFrame {

    public Registro() 
    {
        initComponents();
        setLocationRelativeTo(null);
        setResizable(false);
        setTitle("Registro");
        mostrar();
        
        Image icon = Toolkit.getDefaultToolkit().getImage(getClass().getResource("Imagenes/loguis.png"));
        setIconImage(icon);
        setVisible(true);
        
        
        agregar.setToolTipText("Agregar Empleado");
        eliminar.setToolTipText("Eliminar Empleado Seleccionado");
        modificar.setToolTipText("Modificar Empleado Seleccionado");
        eliminar_todo.setToolTipText("Eliminar Todo");
        volver.setToolTipText("Volver");
        
    }
    
    void mostrar() {
    Tabla_Empleados.getTableHeader().setReorderingAllowed(false);
    DefaultTableModel model = new DefaultTableModel(){
        
    public boolean isCellEditable(int rowIndex,int columnIndex){
        return false;} 
};     
    
        model.addColumn("Codigo");
        model.addColumn("Nombre");
        model.addColumn("Apellido Paterno");
        model.addColumn("Apellido Materno");
        model.addColumn("Sexo");
        model.addColumn("Estado Civil");
        model.addColumn("Telefono");
        model.addColumn("Nacionalidad");
        model.addColumn("Lugar de Nacimiento");
        model.addColumn("Cedula de Identidad");
        model.addColumn("Correo");
        model.addColumn("Fecha de nacimiento");
        
       
       
        
       
        
         Tabla_Empleados.setModel(model);
      
        
        String []datos = new String [11];
        try {
            File f = new File("RegistroEmpleados.txt");
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            String des;
            while((des = br.readLine())!= null){
                StringTokenizer linea = new StringTokenizer(des,"-");
                Vector x = new Vector();
                while(linea.hasMoreTokens()){
                    x.addElement(linea.nextToken());
                }
                model.addRow(x);

          }
        Tabla_Empleados.setModel(model);
        } catch (Exception e) {
        }
    }
   public static String extraerDatos(){
        
        int i =Tabla_Empleados.getSelectedRow(); 
        String Empleados=null;
        boolean band=false;
        
            if(i==-1){ 
               
            }else{
                String codigo=(String)Registro.Tabla_Empleados.getValueAt(i,0); 
                String Nombre=(String)Registro.Tabla_Empleados.getValueAt(i,1); 
                String ApellidoM=(String)Registro.Tabla_Empleados.getValueAt(i, 2);
                String ApellidoP=(String)Registro.Tabla_Empleados.getValueAt(i, 3); 
                String Sexo=(String)Registro.Tabla_Empleados.getValueAt(i, 4); 
                String EstadoCivil=(String)Registro.Tabla_Empleados.getValueAt(i, 5); 
                String Telefono=(String)Registro.Tabla_Empleados.getValueAt(i, 6); 
                String Nacionalidad=(String)Registro.Tabla_Empleados.getValueAt(i, 7);
                String Lugar=(String)Registro.Tabla_Empleados.getValueAt(i, 8);
                String Ci=(String)Registro.Tabla_Empleados.getValueAt(i, 9);
                String Correo=(String)Registro.Tabla_Empleados.getValueAt(i, 10);
                String Fecha=(String)Registro.Tabla_Empleados.getValueAt(i, 11);
                
                Empleados= codigo+"-"+Nombre+"-"+ApellidoM+"-"+ApellidoP+"-"+Sexo+"-"+EstadoCivil+"-"+Telefono+"-"+Nacionalidad+"-"+Lugar+"-"+Ci+"-"+Correo+"-"+Fecha;
                band=true;
                
        } 
            if(band==true){
            return Empleados;
            }else{
                return null;
            }
    }
    
    public void remove(String file, String lineToRemove) {
 
    try {
 
      File inFile = new File(file);
      
      if (!inFile.isFile()) {
        System.out.println("Parameter is not an existing file");
        return;
      }
       
      //Construct the new file that will later be renamed to the original filename. 
      File tempFile = new File(inFile.getAbsolutePath() + ".tmp");
      
      BufferedReader br = new BufferedReader(new FileReader(file));
      PrintWriter pw = new PrintWriter(new FileWriter(tempFile));
      
      String line = null;
 
      //Read from the original file and write to the new 
      //unless content matches data to be removed.
      while ((line = br.readLine()) != null) {
        
        if (!line.trim().equals(lineToRemove)) {
 
          pw.println(line);
          pw.flush();
        }
      }
      pw.close();
      br.close();
      
      //Delete the original file
      if (!inFile.delete()) {
        System.out.println("Could not delete file");
        return;
      } 
      
      //Rename the new file to the filename the original file had.
      if (!tempFile.renameTo(inFile))
        System.out.println("Could not rename file");
      
    }
    catch (FileNotFoundException ex) {
      ex.printStackTrace();
    }
    catch (IOException ex) {
      ex.printStackTrace();
    }
  }
    public static  void borrarFichero(File Ffichero){
     try {
         if(Ffichero.exists()){
           BufferedWriter bw = new BufferedWriter(new FileWriter(Ffichero));
           bw.write("");
           bw.close();
         }
     } catch (Exception ex) {
     }
} 
     void Reseteardid(File Ffichero){
        try {
         if(Ffichero.exists()){
           BufferedWriter bw = new BufferedWriter(new FileWriter(Ffichero));
           bw.write("1");
           bw.close();
         }
     } catch (Exception ex) {
     }
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jLabel1 = new javax.swing.JLabel();
        volver = new javax.swing.JButton();
        minimizar1 = new javax.swing.JButton();
        salir = new javax.swing.JButton();
        agregar = new javax.swing.JButton();
        eliminar = new javax.swing.JButton();
        modificar = new javax.swing.JButton();
        eliminar_todo = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        Tabla_Empleados = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();

        jMenuItem2.setText("Eliminar");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem2);

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Caviar Dreams", 1, 24)); // NOI18N
        jLabel1.setText("Lista de Empleados");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 10, -1, -1));

        volver.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/volverr.png"))); // NOI18N
        volver.setBorder(null);
        volver.setBorderPainted(false);
        volver.setContentAreaFilled(false);
        volver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        volver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                volverActionPerformed(evt);
            }
        });
        getContentPane().add(volver, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 30));

        minimizar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/minimizarr.png"))); // NOI18N
        minimizar1.setBorder(null);
        minimizar1.setBorderPainted(false);
        minimizar1.setContentAreaFilled(false);
        minimizar1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        minimizar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                minimizar1ActionPerformed(evt);
            }
        });
        getContentPane().add(minimizar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 0, 30, 30));

        salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/salir.png"))); // NOI18N
        salir.setBorder(null);
        salir.setBorderPainted(false);
        salir.setContentAreaFilled(false);
        salir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });
        getContentPane().add(salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 0, 30, 30));

        agregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/mas.png"))); // NOI18N
        agregar.setBorder(null);
        agregar.setBorderPainted(false);
        agregar.setContentAreaFilled(false);
        agregar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        agregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agregarActionPerformed(evt);
            }
        });
        getContentPane().add(agregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 40, 40));

        eliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/quitar.png"))); // NOI18N
        eliminar.setBorder(null);
        eliminar.setBorderPainted(false);
        eliminar.setContentAreaFilled(false);
        eliminar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminarActionPerformed(evt);
            }
        });
        getContentPane().add(eliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 90, 40, 40));

        modificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cambiar.png"))); // NOI18N
        modificar.setBorder(null);
        modificar.setBorderPainted(false);
        modificar.setContentAreaFilled(false);
        modificar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        modificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modificarActionPerformed(evt);
            }
        });
        getContentPane().add(modificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 90, 40, 40));

        eliminar_todo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cesta.png"))); // NOI18N
        eliminar_todo.setBorder(null);
        eliminar_todo.setBorderPainted(false);
        eliminar_todo.setContentAreaFilled(false);
        eliminar_todo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        eliminar_todo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminar_todoActionPerformed(evt);
            }
        });
        getContentPane().add(eliminar_todo, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 90, 40, 40));

        Tabla_Empleados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(Tabla_Empleados);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 860, 420));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Fondo_Inventario.png"))); // NOI18N
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 880, 580));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
       
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void volverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_volverActionPerformed
        Admin admin= new Admin();
        this.setVisible(false);
        admin.setVisible(true);
    }//GEN-LAST:event_volverActionPerformed

    private void minimizar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_minimizar1ActionPerformed
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_minimizar1ActionPerformed

    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
        int ax = JOptionPane.showConfirmDialog(null, "¿Desea salir?");
        if(ax == JOptionPane.YES_OPTION){
            System.exit(0);
        }else if(ax == JOptionPane.NO_OPTION){
        }
    }//GEN-LAST:event_salirActionPerformed

    private void agregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agregarActionPerformed
        RegistroEmpleado reg = new RegistroEmpleado();
        reg.setVisible(true);
        dispose();
    }//GEN-LAST:event_agregarActionPerformed

    private void eliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminarActionPerformed
      String Empleados=extraerDatos();
            File file=new File("RegistroEmpleados.txt");
            
            if(Empleados==null){
               JOptionPane.showMessageDialog(null,"Seleccione la fila que desea eliminar");
                }else{
                    
                    int ax = JOptionPane.showConfirmDialog(null, "¿Desea eliminar el cliente?");
                    if(ax == JOptionPane.YES_OPTION){
            
                    remove("RegistroEmpleados.txt",Empleados);
            
                    //Eliminar fila de tabla, solo vista para el usuario
                    DefaultTableModel dtm = (DefaultTableModel) Tabla_Empleados.getModel(); 
                    dtm.removeRow(Tabla_Empleados.getSelectedRow()); 
            
                    JOptionPane.showMessageDialog(null, "Datos del Empleado eliminado con exito.");
                }else if(ax == JOptionPane.NO_OPTION){       
     }   
  }  
    }//GEN-LAST:event_eliminarActionPerformed

    private void modificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modificarActionPerformed
       String Empleados=extraerDatos();
        if(Empleados==null){
          JOptionPane.showMessageDialog(null,"Seleccione la fila que desea modificar");  
        }else{
        Modificar_Empleados E = new Modificar_Empleados();
        E.setVisible(true);
        dispose();
        int i =Tabla_Empleados.getSelectedRow();
        Modificar_Empleados.extraerDatos(i);
        }
        
        
    }//GEN-LAST:event_modificarActionPerformed

    private void eliminar_todoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminar_todoActionPerformed
      
       File id = new File("idEmpleados.txt");
        File file=new File("RegistroEmpleados.txt");
        if (file.delete()){
        System.out.println("El fichero ha sido borrado satisfactoriamente");}
        else{
            System.out.println("No se ha podido eliminar");
        }
        Reseteardid(id);
        mostrar();
    }//GEN-LAST:event_eliminar_todoActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Registro().setVisible(true);
            }
        });
    }
 String codigo,nombre, ApellidoP, ApellidoM, Ci, Correo, Nacionalidad, EstadoCivil, Lugar, Sexo, Telefono, fecha;
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JTable Tabla_Empleados;
    private javax.swing.JButton agregar;
    private javax.swing.JButton eliminar;
    private javax.swing.JButton eliminar_todo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton minimizar1;
    private javax.swing.JButton modificar;
    private javax.swing.JButton salir;
    private javax.swing.JButton volver;
    // End of variables declaration//GEN-END:variables
    String usu,tc;
    String sql;
}
